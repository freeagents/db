CREATE TABLE IF NOT EXISTS "Appointment"
(
    id          UUID         DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    created     TIMESTAMP(0) DEFAULT now(),
    date_start  TIMESTAMP(0)                            NOT NULL,
    apt_user    UUID                                    NOT NULL
        CONSTRAINT user_fk
            REFERENCES "User",
    apt_service UUID                                    NOT NULL
        CONSTRAINT service_fk
            REFERENCES "Service"
);