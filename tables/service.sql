CREATE TABLE IF NOT EXISTS "Service"
(
    id          UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name        VARCHAR(50)                     NOT NULL,
    duration    INT                             NOT NULL,
    description VARCHAR                         NOT NULL,
    photo       VARCHAR,
    price       NUMERIC(10, 2)                  NOT NULL,
    provider    UUID                            NOT NULL
        CONSTRAINT business_fk
            REFERENCES "Business"
);