CREATE TABLE IF NOT EXISTS "User"
(
    id         UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    first_name VARCHAR(50)                     NOT NULL,
    last_name  VARCHAR(50)                     NOT NULL,
    email      VARCHAR(50)                     NOT NULL UNIQUE,
    phone      CHAR(10)                        NOT NULL UNIQUE,
    password   VARCHAR(99)                     NOT NULL,
    zip        INTEGER
);