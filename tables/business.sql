CREATE TABLE IF NOT EXISTS "Business"
(
    id       UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name     VARCHAR(50)                     NOT NULL UNIQUE,
    email    VARCHAR(50)                     NOT NULL UNIQUE,
    password VARCHAR(99)                     NOT NULL,
    street   VARCHAR(50)                     NOT NULL,
    city     VARCHAR(50)                     NOT NULL,
    zip      INTEGER                         NOT NULL,
    OPEN     CHAR(8)                         NOT NULL,
    CLOSE    CHAR(8)                         NOT NULL
);