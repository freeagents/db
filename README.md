# Database for  Project

## Dev Database

Build and then run the database image:
```
sudo docker build -t my-postgres-image .
```
```
sudo docker run -d --name my-postgres-container -p 5432:5432 my-postgres-image
```


### Current Diagram

![Diagram](./assets/diagram.png)
