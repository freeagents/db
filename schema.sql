CREATE EXTENSION "uuid-ossp";

CREATE TABLE IF NOT EXISTS "User"
(
    id         UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    first_name VARCHAR(50)                     NOT NULL,
    last_name  VARCHAR(50)                     NOT NULL,
    email      VARCHAR(50)                     NOT NULL UNIQUE,
    phone      CHAR(10)                        NOT NULL UNIQUE,
    password   VARCHAR(99)                     NOT NULL,
    zip        INTEGER
);

CREATE TABLE IF NOT EXISTS "Business"
(
    id       UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name     VARCHAR(50)                     NOT NULL UNIQUE,
    email    VARCHAR(50)                     NOT NULL UNIQUE,
    password VARCHAR(99)                     NOT NULL,
    street   VARCHAR(50)                     NOT NULL,
    city     VARCHAR(50)                     NOT NULL,
    zip      INTEGER                         NOT NULL,
    OPEN     CHAR(8)                         NOT NULL,
    CLOSE    CHAR(8)                         NOT NULL
);

CREATE TABLE IF NOT EXISTS "Service"
(
    id          UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name        VARCHAR(50)                     NOT NULL,
    duration    INT                             NOT NULL,
    description VARCHAR                         NOT NULL,
    photo       VARCHAR,
    price       NUMERIC(10, 2)                  NOT NULL,
    provider    UUID                            NOT NULL
        CONSTRAINT business_fk
            REFERENCES "Business"
);

CREATE TABLE IF NOT EXISTS "Appointment"
(
    id          UUID         DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    created     TIMESTAMP(0) DEFAULT now(),
    date_start  TIMESTAMP(0)                            NOT NULL,
    apt_user    UUID                                    NOT NULL
        CONSTRAINT user_fk
            REFERENCES "User",
    apt_service UUID                                    NOT NULL
        CONSTRAINT service_fk
            REFERENCES "Service"
);